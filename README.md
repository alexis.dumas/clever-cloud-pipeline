
# 💡☁️ Deploy from GitLab to Clever Cloud

[Clever Cloud](https://www.clever-cloud.com/) is a platform as a service that allows you to deploy, manage and scale your applications. This project contains components to deploy from GitLab to Clever Cloud using [clever-tools](https://www.clever-cloud.com/doc/getting-started/cli/).

## Deploy from GitLab to your production app

To use the `deploy-to-prod` component, you need a running application on Clever Cloud. Add the following snippet to your `.gitlab-ci.yml` file to use in your pipeline:

```yaml
include:
  - component: $CI_SERVER_HOST/<PROJECT_PATH>/deploy-to-prod@~latest
```

Add inputs to override the default values. For example:

```yaml
include:
  - component: $CI_SERVER_HOST/<CI_PROJECT_PATH>/deploy-to-prod@~latest
    inputs:
      stage: test
      ci_commit_branch: '"production"'
```

### `<CI_PROJECT_PATH>` configuration

⚠️ In the project you want to deploy, replace `<CI_PROJECT_PATH>` with the actual path of the components, otherwise this variable fetches your project current path (which doesn't host the CI/CD components). Depending on your platform, path can be different.

#### On gitlab.com

```yaml
- component: $CI_SERVER_HOST/CleverCloud/clever-cloud-pipeline/deploy-to-prod@~latest
```

#### On [Heptapod](https://developers.clever-cloud.com/doc/addons/heptapod/)

```yaml
- component: $CI_SERVER_HOST/pipelines/clever-cloud-pipeline/deploy-to-prod@~latest
```

#### On your self-hosted instance

```yaml
- component: $CI_SERVER_HOST/<group>/<project>/deploy-to-prod@~latest
```

### Component workflow

This script does the following:

- download clever-tools (our CLI) in a Node container
- applies a rule to only execute the workflow if we committed to the default branch (in the case of our project, it's `main`), which you can set in your GitLab project in **Settings > Repository > "Branch defaults"**
- deploys the commit on your production app

### Environment Variables

You need to set some variables in order to be able to use clever-tools in GitLab jobs. Go to your project's **Settings > CI/CD > "Variables"** and add the following variables:

- `CLEVER_TOKEN` AND `CLEVER_SECRET` (find it in your machine, usually in `~/.config/clever-cloud/clever-tools.json`)
- `APP_ID` to avoid committing it (find it at the top right in Clever Cloud Console, in your application tab).

## Review apps from Merge Requests

Use the components to deploy, update and delete a review app from a Merge Request.

### Set the environment variables in your GitLab project

- `TEST_ORG_ID`: the ID of the organization where the review app should be created (we advise that you separate your organisations for production and review apps)

These are the default inputs to run this job. You can modify them to fit your needs.

#### Set environment variables for your review app

If you need to inject environment variables in your review app before deploying it, this is the way to do it:

1. Add them as variables in your repository (**Settings > CI/CD > "Variables"**)
2. Set the `deploy` input to `false`
3. Add this job to your `.gitlab-ci.yml` file:

```yaml
sset environment variables:
  image: node:latest
  stage: deploy
  before_script:
    - npm install -g clever-tools
  script:
    - - clever link -o ${TEST_ORG_ID} ${CI_PROJECT_NAME}-MR-${CI_MERGE_REQUEST_IID} # Replace if you've set a custom app-name input
#   Replace <VARIABLE_NAME> with the actual variable name
    - clever env set <VARIABLE_NAME> "${<VARIABLE_NAME>}"
#   - clever env set... / use this for each variable
    - clever deploy
# Set the job as manual or automatic
  when: manual
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
```

See [this example](https://github.com/CleverCloud/Gitlab-simple-CI-CD-example/blob/main/review-app/set-en-var/.gitlab-ci.yml) for a model.

### Merge Requests components workflow

GitLab jobs dependencies and logic run only on the same pipeline, but you don't need to create a new app for every new commit. Run the jobs manually for review apps as follows:

1. Create review app: run this job only **once** for each MR.
2. Update review app: run this job on every new commit.
3. Delete review app: run this job only **once** when the MR is closed or you simply want to delete the app. If you delete the app and want to execute further jobs on the MR, you need to run `create review app` again.

## Deploy from your self-hosted GitLab instance

You can use this component in your self-hosted GitLab instance. Follow [these instructions](https://docs.gitlab.com/ee/ci/components/index.html#use-a-gitlabcom-component-in-a-self-managed-instance) to set it up.

More info can be found in [GitLab's doc](https://docs.gitlab.com/ee/topics/build_your_application.html)

## 🛟 Troubleshooting

- `ambiguous app name`: if you encounter this error, it means that the name of your app is not unique and you declared the app twice on Clever Cloud. Log in to Clever Cloud console and delete the duplicate app.

If you encounter any issues, please open an issue in this repository.
